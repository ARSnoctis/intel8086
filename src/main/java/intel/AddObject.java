package intel;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddObject {
	private final StringProperty source = new SimpleStringProperty();
	private String sourceValue;
	private final StringProperty destination = new SimpleStringProperty();
	private String destinationValue;
	private final StringProperty data = new SimpleStringProperty();
	private final BooleanProperty valid = new SimpleBooleanProperty(false);
	private static final String LOGGING_PATTERN = "ADD %s, %s\n";
	
	public void setSource(String source) {
		System.out.println("Adding sournce: " + source);
		this.source.set(source);
		validateData();
	}
	
	public void setSourceValue(String value) {
		this.sourceValue = value;
	}

	public String getSource() {
		return this.source.get();
	}
	
	public StringProperty sourceProperty() {
		return source;
	}
	
	public void setDestination(String dest) {
		this.destination.set(dest);
		validateData();
	}
	
	public void setDestinationValue(String value) {
		this.destinationValue = value;
	}
	
	public String getDestination() {
		return destination.get();
	}
	
	public StringProperty destinationProperty() {
		return destination;
	}
	
	@SuppressWarnings("unused")
	private void setData(String data) {
		this.data.set(data);
		validateData();
	}
	
	public String getData() {
		return data.get();
	}
	
	public StringProperty dataProperty() {
		return data;
	}
	
	public void setValid(boolean valid) {
		this.valid.set(valid);
	}
	
	public boolean getValid() {
		return valid.get();
	}
	
	public BooleanProperty validProperty() {
		return valid;
	}
	
	public void validateData() {
		if (source.getValueSafe().isEmpty() || destination.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (data.getValueSafe().isEmpty() && source.get().equals("Data")) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".X") 
				&& data.getValueSafe().length() < 4 
				&& !data.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".[LH]") 
				&& data.getValueSafe().length( )> 2
				&& !data.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".[LH]") 
				&& source.getValueSafe().matches(".X")) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".X") 
				&& source.getValueSafe().matches(".[LH]")) {
			setValid(false);
			return;
		}
		setValid(true);
	}
	
	public String calculate() {
		int destValue = Integer.parseInt(destinationValue, 16);
		int add = getSource().equals("Data")
				? Integer.parseInt(getData(), 16)
						: Integer.parseInt(sourceValue, 16);
		return formatResult(destValue + add);
	}
	
	private String formatResult(int result) {
		String sresult = Integer.toHexString(result);
		if (getDestination().matches(".[LH]") && result > 255) {
			int reduce = result - 256;
			sresult = Integer.toHexString(reduce);
		}
		if (getDestination().matches(".X") && result > 65535) {
			int reduce = result - 65536;
			sresult = Integer.toHexString(reduce);
		}
		return fillZeros(sresult);
	}

	private String fillZeros(String sresult) {
		int zerosToAdd;
		if (getDestination().matches(".X")) {
			zerosToAdd = 4 - sresult.length();
		} else {
			zerosToAdd = 2 - sresult.length();
		}
		for (int i = 1; i <= zerosToAdd; i++) {
			sresult = "0" + sresult;
		}
		return sresult.toUpperCase();
	}

	@Override
	public String toString() {
		if (source.get().equals("Data")) {
			return String.format(LOGGING_PATTERN, getDestination(), "0" + getData() + "h");
		}
		return String.format(LOGGING_PATTERN, getDestination(), getSource());
	}

}
