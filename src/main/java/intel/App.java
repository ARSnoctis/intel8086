/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package intel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {
	
	public static final double WINDOW_SIZE_HEIGHT = 455.0;
	public static final double WINDOW_SIZE_WIDTH = 685.0;
	
	private static final String APP_NAME = "Intel 8086 simulator";


    public static void main(String[] args) {
    	Application.launch(args);
    }

	@Override
	public void start(Stage stage) throws Exception {
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		VBox mainView = FXMLLoader.load(classLoader.getResource("IntelUI.fxml"));
		Scene scene = new Scene(mainView, WINDOW_SIZE_WIDTH, WINDOW_SIZE_HEIGHT);
		scene.getStylesheets().add(classLoader.getResource("IntelUI.css").toExternalForm());
		stage.setScene(scene);
		stage.getIcons().add(new Image("proc.png"));
		stage.setTitle(APP_NAME);
		stage.setResizable(true);
		stage.setMinHeight(494);
		stage.setMinWidth(701);
		stage.show();
	}
}
