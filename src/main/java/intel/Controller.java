package intel;

import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class Controller implements Initializable {
	
	private final MovObject moveOperation = new MovObject();
	private final AddObject addOperation = new AddObject();
	
	private static final List<String> REGISTERS = Arrays.asList(
			"AL", "AH", "AX",
			"BL", "BH", "BX",
			"CL", "CH", "CX",
			"DL", "DH", "DX");
	private static final String REGISTERS_INIT_VALUE = "00";
	private static final String DATA_OPERATION = "Data";
	private static final String OPERATION_DATA_STYLE = "-fx-fill: #4169E1;-fx-font-weight:bold;";
	private static final String OPERATION_ENTRY_STYLE = "-fx-fill: BLACK;-fx-font-weight:bold;";
	private static final String FIELD_VALIDATOR_PATTERN = "[a-fA-F0-9]{2}|[a-fA-F0-9]{4}";
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("'['yyyy.MM.dd HH:mm:ss z'] '");
	private static HashMap<String, TextField> registersMap = new HashMap<String, TextField>();
	
	@FXML
	private TextField ah;
	
	@FXML
	private TextField al;
	
	@FXML
	private TextField bh;
	
	@FXML
	private TextField bl;
	
	@FXML
	private TextField ch;
	
	@FXML
	private TextField cl;
		
	@FXML
	private TextField dh;
	
	@FXML
	private TextField dl;
	
	@FXML
	private ComboBox<String> mov_dsource;
	
	@FXML
	private TextField mov_data;

	@FXML
	private ComboBox<String> mov_dest;
	
	@FXML
	private Button mov_exec;
	
	@FXML
	private ComboBox<String> add_dsource;
	
	@FXML
	private TextField add_data;

	@FXML
	private ComboBox<String> add_dest;
	
	@FXML
	private Button add_exec;
	
	@FXML
	private TextFlow logger;
	
	@FXML
	private ScrollPane scrollBox;
	
	@FXML
	private MenuItem aboutMenu;
	

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		//initialize mov tab comboboxes
		List<String> registers = new ArrayList<>(REGISTERS);
		mov_dest.getItems().addAll(registers);
		add_dest.getItems().addAll(registers);
		registers.add(DATA_OPERATION);
		mov_dsource.getItems().addAll(registers);
		add_dsource.getItems().addAll(registers);
		
		//Initialize register values
		ah.setText(REGISTERS_INIT_VALUE);
		al.setText(REGISTERS_INIT_VALUE);
		bh.setText(REGISTERS_INIT_VALUE);
		bl.setText(REGISTERS_INIT_VALUE);
		ch.setText(REGISTERS_INIT_VALUE);
		cl.setText(REGISTERS_INIT_VALUE);
		dh.setText(REGISTERS_INIT_VALUE);
		dl.setText(REGISTERS_INIT_VALUE);
		registersMap.put("AH", ah);
		registersMap.put("AL", al);
		registersMap.put("BL", bl);
		registersMap.put("BH", bh);
		registersMap.put("CL", cl);
		registersMap.put("CH", ch);
		registersMap.put("DL", dl);
		registersMap.put("DH", dh);
		
		//init value in log
		Text initLogEntry = new Text("Opperations logger\n");
		initLogEntry.setStyle(OPERATION_ENTRY_STYLE);
		logger.getChildren().add(initLogEntry);
		
		//mov_dsource listener definition 
		mov_dsource.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(DATA_OPERATION)) {
				mov_data.setDisable(false);
			} else {
				mov_data.clear();
				mov_data.setDisable(true);
			}
		});
		
		//add_dsource listener definition 
		add_dsource.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(DATA_OPERATION)) {
				addOperation.setSourceValue("");
				add_data.setDisable(false);
			} else {
				addOperation.setSourceValue(getRegisterValue(newValue));
				add_data.clear();
				add_data.setDisable(true);
			}
		});
		
		//add_dest listener definition 
		add_dest.valueProperty().addListener((observable, oldValue, newValue) -> 
			addOperation.setDestinationValue(getRegisterValue(newValue)));
		
		//triggers mov
		mov_dsource.setOnAction(e -> moveOperation.validateData());
		mov_dest.setOnAction(e -> moveOperation.validateData());
		mov_data.setOnKeyTyped(e -> moveOperation.validateData());
		
		aboutMenu.setOnAction(e -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Application information");
			alert.setHeaderText(null);
			alert.setContentText("Thanks for using this application!\n\tversion: 1.0.0\n\tAuthor: Grzegorz Flakowicz");
			alert.showAndWait();
		});
		
		//triggers add
		add_dsource.setOnAction(e -> addOperation.validateData());
		add_dest.setOnAction(e -> addOperation.validateData());
		add_data.setOnKeyTyped(e -> addOperation.validateData());
		
		//bindings mov
		mov_dsource.valueProperty().bindBidirectional(moveOperation.sourceProperty());
		mov_dest.valueProperty().bindBidirectional(moveOperation.destinationProperty());
		mov_data.textProperty().bindBidirectional(moveOperation.dataProperty());
		
		//bindings add
		add_dsource.valueProperty().bindBidirectional(addOperation.sourceProperty());
		add_dest.valueProperty().bindBidirectional(addOperation.destinationProperty());
		add_data.textProperty().bindBidirectional(addOperation.dataProperty());
		
		//mov operation validator listener
		moveOperation.validProperty().addListener((observable,  oldValue,  newValue) -> {
			if (newValue) mov_exec.setDisable(false);
			else {
				mov_exec.setDisable(true);
			}
		});
		
		//add operation validator listener
		addOperation.validProperty().addListener((observable,  oldValue,  newValue) -> {
			if (newValue) add_exec.setDisable(false);
			else {
				add_exec.setDisable(true);
			}
		});
		
		//mov data entry validator
		mov_data.focusedProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue) {
				if (!mov_data.getText().matches(FIELD_VALIDATOR_PATTERN)) {
					mov_data.setText("");
                } else {
                	String fieldText = mov_data.getText();
                	mov_data.setText(fieldText.toUpperCase());
                }
			}
			moveOperation.validateData();
		});
		
		//add data entry validator
		add_data.focusedProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue) {
				if (!add_data.getText().matches(FIELD_VALIDATOR_PATTERN)) {
					add_data.setText("");
                } else {
                	String fieldText = add_data.getText();
                	add_data.setText(fieldText.toUpperCase());
                }
			}
			addOperation.validateData();
		});
		
		
		//Textflow resizer height
		scrollBox.heightProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				double diff = newValue.doubleValue() - oldValue.doubleValue();
				double loggerHeight = logger.getHeight();
				logger.setMinHeight(loggerHeight + diff);
			}
		});
		
		//Textflow resizer width
		scrollBox.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				double diff = newValue.doubleValue() - oldValue.doubleValue();
				if (diff != App.WINDOW_SIZE_WIDTH) {
					double loggerWidth = logger.getWidth();
					logger.setMinWidth(loggerWidth + diff);
				}
			}
		});
		
	}
	
	@FXML
	private void aboutEvent(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setHeaderText("Are you sure you want to cancel?");
		alert.setTitle("Cancelling Update");
	}

	@FXML 
	private void addExecutionButtonClicked(ActionEvent event) {
		if(!addOperation.getSource().equals("Data")) {
			addOperation.setSourceValue(getRegisterValue(addOperation.getSource()));
		}
		addOperation.setDestinationValue(getRegisterValue(addOperation.getDestination()));
		if(addOperation.getValid()
				&& addOperation.getDestination().matches(".[LH]")) {
			copyData(addOperation.calculate(), addOperation.getDestination());
		}
		if(addOperation.getValid()
				&& addOperation.getDestination().matches(".X")) {
			copyLongData(addOperation.calculate(), addOperation.getDestination());
		}
		Text date = new Text(ZonedDateTime.now().format(FORMATTER));
		date.setStyle(OPERATION_DATA_STYLE);
		Text operation = new Text(addOperation.toString());
		operation.setStyle(OPERATION_ENTRY_STYLE);
		logger.getChildren().addAll(date, operation);
	}
	
	@FXML
	private void movExecutionButtonClicked(ActionEvent event) {
				
		if (moveOperation.getValid() 
				&& !moveOperation.dataProperty().getValueSafe().isEmpty() 
				&& moveOperation.getDestination().matches(".[LH]")) {
			copyData(moveOperation.getData(), moveOperation.getDestination());
		}
		if (moveOperation.getValid() 
				&& !moveOperation.dataProperty().getValueSafe().isEmpty() 
				&& moveOperation.getDestination().matches(".X")) {
			copyLongData(moveOperation.getData(), moveOperation.getDestination());
		}
		if (moveOperation.getValid()
				&& moveOperation.dataProperty().getValueSafe().isEmpty()
				&& moveOperation.getDestination().matches(".[LH]")) {
			copyRegisters(moveOperation.getSource(), moveOperation.getDestination());
		}
		if (moveOperation.getValid()
				&& moveOperation.dataProperty().getValueSafe().isEmpty()
				&& moveOperation.getDestination().matches(".X")) {
			copyLongRegisters(moveOperation.getSource(), moveOperation.getDestination());
		}
		Text date = new Text(ZonedDateTime.now().format(FORMATTER));
		date.setStyle(OPERATION_DATA_STYLE);
		Text operation = new Text(moveOperation.toString());
		operation.setStyle(OPERATION_ENTRY_STYLE);
		logger.getChildren().addAll(date, operation);
	}

	private void copyLongRegisters(String source, String destination) {
		String lowSourceRegister = source.replaceAll("X", "L");
		String highSourceRegister = source.replaceAll("X", "H");
		String lowDestRegister = destination.replaceAll("X", "L");
		String highDestRegister = destination.replaceAll("X", "H");
		copyRegisters(lowSourceRegister, lowDestRegister);
		copyRegisters(highSourceRegister, highDestRegister);
	}

	private void copyRegisters(String source, String destination) {
		TextField sourceRegister = registersMap.get(source);
		TextField destRegister = registersMap.get(destination);
		destRegister.setText(sourceRegister.getText());
		
	}

	private void copyLongData(String data, String doubleRegister) {
		String lowRegister = doubleRegister.replaceAll("X", "L");
		String lowData = data.substring(2, 4);
		copyData(lowData, lowRegister);
		String highRegister = doubleRegister.replaceAll("X", "H");
		String highData = data.substring(0, 2);
		copyData(highData, highRegister);
		
	}

	private void copyData(String data, String singleRegister) {
		TextField register = registersMap.get(singleRegister);
		register.setText(data);
	}
	
	private String getRegisterValue(String register) {
		if (register.matches(".X")) {
			String lowRegister = register.replaceAll("X", "L");
			String highRegister = register.replaceAll("X", "H");
			return registersMap.get(highRegister).getText() 
					+ registersMap.get(lowRegister).getText();
		}
		return registersMap.get(register).getText();
	}

}
