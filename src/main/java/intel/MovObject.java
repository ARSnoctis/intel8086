package intel;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class MovObject {
	
	private final StringProperty source = new SimpleStringProperty();
	private final StringProperty destination = new SimpleStringProperty();
	private final StringProperty data = new SimpleStringProperty();
	private final BooleanProperty valid = new SimpleBooleanProperty(false);
	private static final String LOGGING_PATTERN = "MOV %s, %s\n";
	
	public void setSource(String source) {
		System.out.println("Adding sournce: " + source);
		this.source.set(source);
		validateData();
	}
	
	public String getSource() {
		return this.source.get();
	}
	
	public StringProperty sourceProperty() {
		return source;
	}
	
	public void setDestination(String dest) {
		this.destination.set(dest);
		validateData();
	}
	
	public String getDestination() {
		return destination.get();
	}
	
	public StringProperty destinationProperty() {
		return destination;
	}
	
	@SuppressWarnings("unused")
	private void setData(String data) {
		this.data.set(data);
		validateData();
	}
	
	public String getData() {
		return data.get();
	}
	
	public StringProperty dataProperty() {
		return data;
	}
	
	public void setValid(boolean valid) {
		this.valid.set(valid);
	}
	
	public boolean getValid() {
		return valid.get();
	}
	
	public BooleanProperty validProperty() {
		return valid;
	}
	
	public void validateData() {
		if (source.getValueSafe().isEmpty() || destination.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (data.getValueSafe().isEmpty() && source.get().equals("Data")) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".X") 
				&& data.getValueSafe().length() < 4 
				&& !data.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".[LH]") 
				&& data.getValueSafe().length() > 2
				&& !data.getValueSafe().isEmpty()) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".[LH]") 
				&& source.getValueSafe().matches(".X")) {
			setValid(false);
			return;
		}
		if (destination.getValueSafe().matches(".X") 
				&& source.getValueSafe().matches(".[LH]")) {
			setValid(false);
			return;
		}
		setValid(true);
	}
	
	@Override
	public String toString() {
		if (source.get().equals("Data")) {
			return String.format(LOGGING_PATTERN, getDestination(), "0" + getData() + "h");
		}
		return String.format(LOGGING_PATTERN, getSource(), getDestination());
	}
}
